#! /usr/bin/env bash
# env  variables come from docker file, replace the values set in Dockerfile with those in  nginx config file
envsubst '$PROXY_PROTOCOL,$PROXY_UPSTREAM' < /etc/nginx/conf.d/default.template > /etc/nginx/conf.d/default.conf


# make command executed dynamically using custom command (passing in command line arguments)
exec "$@"
