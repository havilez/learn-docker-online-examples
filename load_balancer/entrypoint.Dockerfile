FROM nginx:latest

# install  envsubst is part of the gettext-base package
RUN apt-get update && apt-get install gettext-base


# set defaults for environment variables to be used
ENV PROXY_PROTOCOL=http PROXY_UPSTREAM=example.com

# this is a template .conf file as variables have not been substitued with values yet
COPY proxy.conf /etc/nginx/conf.d/default.template

# shell script will  replace the envrionment variables used in .conf file
## with values specified in script  @ runtime

COPY start2.sh /

CMD ["/start2.sh"]

#### removed from start.sh exec nginx -g 'daemon off;'
ENTRYPOINT [ "nginx" ,"-g", "daemon off"]