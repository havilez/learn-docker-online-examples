#!/usr/bin/env bash
envsubst '$PROXY_PROTOCOL,$PROXY_UPSTREAM' < /etc/nginx/conf.d/default.template > /etc/nginx/conf.d/default.conf

# exec replaces bash shell with command specified
# this causes the specified command to have PID 1
# when process has PID 1 the signals are passed from process to the container to handle
# i.e. SIG_KILL, SIG_TEMINATE , etc
####exec nginx -g 'daemon off;'
